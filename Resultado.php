<?php
    include ("Conexao.php");
    
    require_once "Cientifica.php";

    class Resultado extends Conexao{
        private $funcao;
        private $resultado;
        private $formula;
        
        /*public function __construct($funcao,$resultado,$texto,$numero_1,$numero_2,$numero_3){
            $this->funcao = $funcao;
            $this->resultado=$resultado;
            $this->texto=$texto;
            $this->numero_1=$numero_1;
            $this->numero_2=$numero_2;
            $this->numero_3=$numero_3;

        }*/
        public function __set($p,$v){
            $this->$p = $v;
        }
    
        public function __get($p){
            return $this->$p;
        }

        public function salvar($numero_1,$numero_2,$numero_3){
            $sql = "INSERT INTO resultado(formula,numero_1,numero_2,numero_3,resultado) VALUES(:formula,:numero_1,:numero_2,:numero_3,:resultado)";
            echo $sql;
            $param = array(
                ":formula"=>$this->formula,
                ":numero_1"=>$numero_1,
                ":numero_2"=>$numero_2,
                ":numero_3"=>$numero_3,
                ":resultado"=>$this->resultado
            );
            $id = parent::inserir($sql,$param);
            //Adicionada linha para definir o ID retornado ao atributo da classe
            
            echo "Inserido com ID: ".$id;
        }
        
        public function calcular($formula,$numero_1,$numero_2,$numero_3=null){
            
            $cient = new Cientifica();
            echo "<br>Insira a opção desejada:";
            echo "<br>fatorial";
            echo "<br>baskara";
            echo "<br>funcao";

            $this->formula = $formula;

            switch ($formula) {
                case 'fatorial':
                    $this->resultado = $cient->fatorial($numero_1,$numero_2);
                    $this->salvar($numero_1,$numero_2,$numero_3); 
                    break;
                case 'baskara':
                    $this->resultado = $cient->baskara($numero_1,$numero_2,$numero_3);
                    $this->salvar($numero_1,$numero_2,$numero_3); 
                    break;
                case 'funcao':
                    $this->resultado = $cient->funcao($numero_1);
                    $this->salvar($numero_3); 
                    break;
                default:
                    echo "Opção Invalida.";
                    break;
            }

        }

    }

?>