<?php
    include "Calculadora.php";
    class  Cientifica extends Calculadora{

        private $numero_3;

        public function __construct (){
            parent::__construct();
        }

        public function __set($p,$v){
            $this->$p = $v;
        }
    
        public function __get($p){
            return $this->$p;
        }

        public function fatorial($numero_1,$numero_2){
            $this->numero_3 = $this->somar($numero_1,$numero_2);
            $res = 1;
            for ($i = 1 ; $i <= $this->numero_3; $i++) {
                $res *= $i;
            }
            return $res;
        }
        public function baskara($numero_3,$numero_1,$numero_2){

            $bquad = ($this->multiplicacao($numero_2,$numero_2));
            $delta = ($bquad)-((4*$numero_1)*$numero_3);

            $x1 = (-$numero_2 + sqrt ($delta)) / (2 * $numero_1);
            $x2 = (-$numero_2 - sqrt ($delta)) / (2 * $numero_1);
            echo "<br>".$x1;
            echo "<br>".$x2;

        }
        public function funcao($numero_3){
            $raiz = sqrt($numero_3);
            return $raiz;
        }
    }

?>